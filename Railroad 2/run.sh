rustc main.rs

echo ">>> Sample 1: "
echo "Input: "
cat 1.in
echo "Answer: "
cat 1.ans
echo "";
./main < 1.in
echo "";

echo ">>> Sample 2: "
echo "Input: "
cat 2.in
echo "Answer: "
cat 2.ans
echo "";
./main < 2.in
echo "";

echo ">>> Sample 3: "
echo "Input: "
cat 3.in
echo "Answer: "
cat 3.ans
echo "";
./main < 3.in
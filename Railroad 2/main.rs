use std::io;

fn main() {
  let mut line = String::new();
  io::stdin().read_line(&mut line).unwrap();

  let data: Vec<&str> = line.split_whitespace().collect();
  let x = data[0].parse::<i64>().unwrap();
  let y = data[1].parse::<i64>().unwrap();

  eprintln!("x: {}, y: {}", x, y);

  if y % 2 != 0 {
    println!("impossible");
  } else {
    println!("possible");
  }
}
#include <iostream>
#include <sstream>
#include <vector>
#include <queue>

using namespace std;

class Relation
{
public:
  string child;
  string parent1;
  string parent2;
};

class Person
{
public:
  string name;
  Person *parents[2];
  vector<Person *> children;
  float royal;

  Person(string _n)
  {
    name = _n;
    parents[0] = nullptr;
    parents[1] = nullptr;
    royal = 0.0f;
  }
};

void uniqueAdd(vector<Person *> &persons, string name)
{
  bool isUnique = true;
  for (Person *other : persons)
  {
    if (other->name == name)
    {
      isUnique = false;
      break;
    }
  }
  if (isUnique)
  {
    persons.push_back(new Person(name));
  }
  else
  {
  }
}

Person *getPerson(const vector<Person *> &persons, string name)
{
  for (Person *other : persons)
  {
    if (other->name == name)
    {
      return other;
    }
  }
  return nullptr;
}

vector<Person *> BFS(Person *person)
{
  vector<Person *> result;
  vector<Person *> disc;
  queue<Person *> q;
  q.push(person);

  while (!q.empty())
  {
    Person *p = q.front();
    q.pop();

    for (Person *child : p->children)
    {
      bool isDisc = false;
      for (Person *d : disc)
      {
        if (d == child)
        {
          isDisc = true;
        }
      }
      if (isDisc)
        continue;

      disc.push_back(child);
      q.push(child);
      result.push_back(child);
      cerr << child->name << endl;
    }
  }

  return result;
}

int main()
{
  cin.tie(NULL);

  /**
   * INPUT DATA
   */
  cerr << "INPUT:" << endl;
  
  long numRelations, numClaimants;
  cin >> numRelations >> numClaimants;
  cerr << numRelations << " " << numClaimants << endl; // debug

  string leader;
  cin >> leader;
  cerr << leader << endl; // debug

  vector<Relation> relations(numRelations);
  for (long i = 0; i < numRelations; i++)
  {
    string child, parent1, parent2;
    cin >> child >> parent1 >> parent2;
    cerr << child << " " << parent1 << " " << parent2 << endl; // debug
    // cerr << parent1 << " --> " << child << endl;
    // cerr << parent2 << " --> " << child << endl;
    Relation relation;
    relation.child = child;
    relation.parent1 = parent1;
    relation.parent2 = parent2;
    relations[i] = relation;
  }
  
  vector<string> claimants(numClaimants);
  for (long i = 0; i < numClaimants; i++)
  {
    string claimant;
    cin >> claimant;
    cerr << claimant << endl;
    claimants[i] = claimant;
  }

  /**
   * BUILD TREE
   */
  cerr << endl
       << "BUILD TREE:" << endl;
  
  vector<Person *> persons;

  // first iteration adds all persons
  uniqueAdd(persons, leader);
  persons[0]->royal = 1.0f;
  for (Relation relation : relations)
  {
    uniqueAdd(persons, relation.child);
    uniqueAdd(persons, relation.parent1);
    uniqueAdd(persons, relation.parent2);
  }

  // second iteration connects parents
  for (Relation relation : relations)
  {
    Person *child = getPerson(persons, relation.child);
    Person *parent1 = getPerson(persons, relation.parent1);
    Person *parent2 = getPerson(persons, relation.parent2);

    child->parents[0] = parent1;
    child->parents[1] = parent2;
    parent1->children.push_back(child);
    parent2->children.push_back(child);
  }

  vector<Person *> bfs = BFS(persons[0]);

  /**
   * CALCULATE ROYALNESS
   */
  cerr << endl
       << "CALCULATE ROYALNESS:" << endl;

  for (Person *person : bfs)
  {
    if (person->parents[0] != nullptr && person->parents[1] != nullptr)
    {
      person->royal += (person->parents[0]->royal * 0.5f);
      person->royal += (person->parents[1]->royal * 0.5f);
    }

    cerr << person->name << " " << person->royal << endl;
  }

  /**
   * CALCULATE HEIR
   */
  cerr << endl
       << "CALCULATE HEIR:" << endl;

  float recordRoyal = 0.0f;
  long recordIndex = -1;
  for (long i = 0; i < numClaimants; i++)
  {
    Person *person = getPerson(persons, claimants[i]);
    if (person != nullptr && person->royal > recordRoyal)
    {
      recordRoyal = person->royal;
      recordIndex = i;
    }
  }
  cout << claimants[recordIndex] << endl;

  /**
   * CLEANUP
   */
  for (long i = 0; i < persons.size(); i++)
  {
    delete persons[i];
  }
}
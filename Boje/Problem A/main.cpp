#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

long numCases;
int main()
{
  cin.tie(NULL);

  cin >> numCases;

  long numGuests;
  for (long i = 0; i < numCases; i++)
  {
    cin >> numGuests;

    vector<long> guestList(numGuests, -1);
    long guestCode;
    for (long j = 0; j < numGuests; j++)
    {
      cin >> guestCode;
      guestList[j] = guestCode;
    }

    sort(guestList.begin(), guestList.end());
    long loneGuest = -1;
    for (long j = 0; j < guestList.size(); j++)
    {
      long current = guestList[j];
      if (j > 0)
      {
        long prev = guestList[j - 1];
        if (current == prev)
          continue;
      }
      if (j < guestList.size() - 1)
      {
        long next = guestList[j + 1];
        if (current == next)
          continue;
      }
      loneGuest = guestList[j];
      break;
    }

    cout << "Case #" << i + 1 << ": " << loneGuest << endl;
  }
}
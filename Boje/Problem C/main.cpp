#include <iostream>
#include <vector>
#include <math.h>

using namespace std;

int main()
{
  cin.tie(NULL);

  /**
   * INPUT
   */
  cerr << "INPUT:" << endl;

  long setLen;
  cin >> setLen;
  cerr << setLen << endl;

  vector<long> set(setLen);
  for (long i = 0; i < setLen; i++)
  {
    long value;
    cin >> value;
    set[i] = value;
    cerr << value << " ";
  }
  cerr << endl;

  /**
   * TEST
   */
  cerr << endl
       << "TEST:" << endl;

  vector<long> recordSubset;
  long recordValue = 0;

  unsigned int i, j;
  unsigned int bits;
  unsigned short bit;
  vector<long> subset;
  long value;
  for (i = 1; i < pow(2, set.size()); i++)
  {
    bits = floor(log2(i));
    cerr << "{ ";

    subset.clear();
    for (j = 0; j <= bits; j++)
    {
      bit = (i >> j) & 1;
      if (bit == 1)
      {
        subset.push_back(set[j]);
        cerr << set[j] << " ";
      }
    }

    value = subset[0];
    for (j = 1; j < subset.size(); j++)
    {
      value = value ^ set[j];
    }

    if (value > recordValue)
    {
      recordValue = value;
      recordSubset = subset;
    }

    cerr << "} = " << value << endl;
  }
  cerr << endl;

  /**
   * BEST
   */
  cerr << "BEST:" << endl;
  cerr << "{ ";
  for (long i : recordSubset)
  {
    cerr << i << " ";
  }
  cerr << "} = ";
  cout << recordValue << endl;
}
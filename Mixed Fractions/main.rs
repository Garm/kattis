use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();

  for line in stdin.lock().lines().map(|l| l.unwrap()) {
    let nums: Vec<i64> = line.split_whitespace()
      .map(|num| num.parse::<i64>().unwrap())
      .collect();
    
    if nums == &[0, 0] {
      return;
    }

    let whole = nums[0] / nums[1];
    let numerator = nums[0] % nums[1];
    
    println!("{} {} / {}", whole, numerator, nums[1]);
  }
}
#include <iostream>
#include <string>
#include <math.h>

using namespace std;

string input;
unsigned long long inputSize;
unsigned long long numWildcards = 0;

int main()
{
  cin.tie(NULL);

  /**
   * INPUT
   */
  
  cin >> input;
  inputSize = input.size();
  
  

  /**
   * NUMBER OF ?s
   */
  
  for (char ch : input)
  {
    if (ch == '?')
    {
      numWildcards++;
    }
  }
  
  

  /**
   * PERMUTATIONS
   */
  

  unsigned long long s = exp2(numWildcards);

  unsigned long long i, j, k, m;
  unsigned short bit;
  string combo;
  unsigned long long inverts = 0;
  unsigned long long totalInverts = 0;
  for (i = 0; i < s; i++)
  {
    inverts = 0;
    k = 0, m = 0;
    for (j = inputSize; j > 0; j--)
    {
      char ch = input[(j - 1)];
      if (ch == '?')
      {
        bit = (i >> k) & 1;
        if (bit == 0)
        {
          ch = '0';
        }
        else if (bit == 1)
        {
          ch = '1';
        }
        k++;
      }

      if (ch == '0')
      {
        m++;
      }
      else if (ch == '1')
      {
        inverts += m;
      }

      
    }
    

    totalInverts += inverts % 1000000007;
  }
  
  

  /**
   * ANSWER:
   */
  

  
  cout << totalInverts << endl;
}
use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();

  let lines: Vec<String> = stdin.lock().lines().map(|l| l.unwrap()).collect();

  let num = lines[0].parse::<usize>().unwrap();

  for i in 1..num+1 {
    let size = f64::sqrt(lines[i].len() as f64) as usize;
    let chars: Vec<char> = lines[i].chars().collect();

    let mut result = String::new();
    for j in 0..size {
      for k in 0..size {
        let x = k;
        let y = size - 1 - j;
        let index = y + x * size;
        
        result.push(chars[index]);
      }
    }
    println!("{}", result);
  }
}
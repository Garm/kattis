use std::io;

fn main() {
  let stdin = io::stdin();


  let mut line = String::new();
  match stdin.read_line(&mut line) {
    Ok(_n) => {
      line = line.trim_end().to_string();
    }
    Err(err) => panic!(err),
  }

  let num = line.parse::<i64>().unwrap();
  eprintln!("Sequence Length: {}", num);

  let mut sum: i64 = 0;
  for _i in 0..num {
    line = String::new();
    match stdin.read_line(&mut line) {
      Ok(_n) => {
        line = line.trim_end().to_string();
      }
      Err(err) => panic!(err),
    }

    let base = line[..line.len()-1].parse::<i64>().unwrap();
    let exponent = line[line.len()-1..].parse::<u32>().unwrap();
    let power = base.pow(exponent);
    eprintln!("{}: {}^{} = {}", _i, base, exponent, power);

    sum += power;
  }
  eprint!("Total Sum: ");
  println!("{}", sum);
}
#include <iostream>

using namespace std;

long h = -1, t = -1;
long numMoves = 0;

void move(int id)
{
  long dh = 0, dt = 0;

  switch (id)
  {
  case 1:
    break;

  case 2:
    dt = 1;
    break;

  case 3:
    dh = -2;
    break;

  case 4:
    dh = 1;
    dt = -2;
    break;
  }

  h += dh;
  t += dt;
  numMoves++;

  cerr << "Use " << id << ", " << h << " heads and " << t << " tails remain." << endl;
}

/**
 * Behaviour priority:
 * 1. Get even tails with move 2
 * 2. Cut tails until zero with move 4
 * 3. Cut heads until zero or one with move 3
 * if Head == 0
 *  4. Win
 * else
 *  4. Add tails with move 2 twice
 *  5. Cut the tails with move 4
 *  6. Kill with move 3
 */

int main()
{
  cin.tie(NULL);

  while (cin >> h >> t)
  {
    numMoves = 0;
    if (h == 0 && t == 0)
    {
      break;
    }
    cerr << "A wild hydra appears!" << endl;
    cerr << "It has " << h << " heads and " << t << " tails." << endl;

    // get tails even
    if (t % 2 == 1)
    {
      move(2);
    }

    // cut the tails!
    while (t >= 2)
    {
      move(4);
    }

    // off with theirs heads!
    while (h >= 2)
    {
      move(3);
    }

    // what if he's not dead?
    // do the killing stroke!
    if (h == 1)
    {
      move(2);
      move(2);
      move(4);
      move(3);
    }

    if (h == 0 && t == 0)
    {
      cerr << "The hydra is dead!" << endl;
    }
    cerr << "Moves made: ";

    cout << numMoves << endl;
    cerr << endl;
  }
}
#include <iostream>

using namespace std;

int main()
{
    cin.tie(NULL);

    long N;
    cin >> N;

    if (N % 2 == 0)
    {
        cout << "Bob" << endl;
    }
    else
    {
        cout << "Alice" << endl;
    }
}
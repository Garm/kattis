#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <stack>
#include <cctype>
#include <algorithm>

using namespace std;

bool isop(char c)
{
    return c == '+' || c == '-' || c == '*';
}

int to_int(char const *s, int &result)
{
    bool negate = (s[0] == '-');
    if (*s == '+' || *s == '-')
    {
        ++s;
    }

    result = 0;
    while (*s)
    {
        if (*s >= '0' && *s <= '9')
        {
            result = result * 10 - (*s - '0');
        }
        else
        {
            return false;
        }
        ++s;
    }

    if (!negate)
    {
        result = -result;
    }

    return true;
}

int main()
{
    cin.tie(NULL);

    string in;

    while (!cin.eof())
    {
        getline(cin, in);

        cerr << in << endl;

        stack<string> words;
        string cur_word = "";
        for (size_t i = in.size(); i > 0; i--)
        {
            char c = in[i - 1];

            if (c != ' ')
            {
                if (isop(c))
                {
                    string left = words.top();
                    words.pop();
                    string right = words.top();
                    words.pop();

                    int ileft, iright, iresult;
                    if (to_int(left.c_str(), ileft) && to_int(right.c_str(), iright))
                    {
                        switch (c) 
                        {
                            case '+':
                                iresult = ileft + iright;
                                break;
                            
                            case '-':
                                iresult = ileft - iright;
                                break;
                            
                            case '*':
                                iresult = ileft * iright;
                                break;
                        }
                        cerr << ileft << c << iright << '=' << iresult << endl;
                        words.push(to_string(iresult));
                    }
                }
                else
                {
                    cur_word += c;
                }
            }

            // if (c != ' ')
            // {
            //     cur_word += c;
            // }

            if (c == ' ' || i == in.size() - 1)
            {
                // reverse since we're stepping back to front
                reverse(cur_word.begin(), cur_word.end());

                words.push(cur_word);
                cur_word = "";
            }
        }
    }
}
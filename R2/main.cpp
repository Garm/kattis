#include <iostream>

using namespace std;

int main()
{
  cin.tie(NULL);

  cerr << "INPUT:" << endl;

  long r1, s;
  cin >> r1 >> s;
  cerr << r1 << " " << s << endl;

  long answer = 2 * s - r1;
  cout << answer << endl;
}
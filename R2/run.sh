# compile
g++ -g -O2 -static -std=gnu++17 main.cpp -o main

# test
echo SAMPLE \#1
./main < 1.in
echo
echo SAMPLE \#2
./main < 2.in